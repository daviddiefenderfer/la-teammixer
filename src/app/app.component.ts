import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public teamSize = 3;
  public members: string[] = [
    'Ryan Beytell',
    'David Diefenderfer',
    'Lakin Ducker',
    'Jonathan Fricke',
    'Chris Garver',
    'James Hoegerl',
    'Jerome Johnson Jr.',
    'Richard Layton',
    'Kurt Maine',
    'James Mattingly',
    'Mitch McCutchen',
    'Patrick O\'Sheal',
    'Timothy Peacock',
    'Matt Philpot',
    'Sean Reimer',
    'Markim Shaw',
    'Richard Stovall',
    'Scott Travis',
    'Will Trott',
    'Roberto Villatoro',
    'Jeff Ward',
    'Dennis Worsham',
    'Jon Lee'
  ];

  public shuffleTeams() {
    const a = [...this.members];
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }

    this.members = a;
  }
}
